# CarCar

Team:

* Nicholas Trevino - Service/ Split Inventory through pair programming
* Garrett Hoffman - Sales/ Split Inventory through pair programming

</br>

## Table of Contents

[TOC]

## Design
CarCar is an application designed for managing aspects of an automobile dealership, specifically its inventory, service center, and sales. The dealership was split into three microservices pertaining to the inventory, service center, and sales.  Inventory handles automobiles along with their make and model. The service microservice will handle service appointments scheduling, vehicle service history, and technicians who will work on the car while polling Inventory microservice for automobile information. Sales will handle sales records, sales employees, and customers in addition to polling the Inventory microservice for automobile information.

</br>
</br>

## Instructions
***How to Run***

- Fork and clone the project

- ```cd``` into the newly cloned directory and run the following commands:

1. Run ```docker volume create beta-data```
2. Run ```docker compose build```
3. Run ```docker compose up```

- It may take around 5 minutes for the react development server to be fully functional at ```http://localhost:3000```
- Use Navigation links at the top of the webpage to maneuver around the application
- Insomnia is a great resource to access the backend RESTful APIs listed in the below microservices

***Troubleshooting Database***

You may need to redo your database. You can follow these steps if needed:

1. Stop all services
2. Run ```docker container prune -f```
3. Run ```docker volume rm beta-data```
4. Run ```docker volume create beta-data```
5. Run ```docker compose build```
6. Run ```docker compose up```

## Diagram
![image](/images/ProjectBetaDesign.png/)

</br>
</br>

## Inventory microservice
There are three models associated with the Inventory microservice

1. Manufacturer - Entity

    - Requires properties = [ "name" ]

2. Vehicle Model - Entity

    - Requires properties = [ "name", "picture_url", "manufacturer_ForeignKey" ]

3. Automobile - Entity

    - Requires properties = [ "color", "year", "VIN", "model_ForeignKey" ]

    - The VIN property field requires 17 characters

__Inventory Port: 8100__

***Manufacturer RESTful API endpoints***

| Action                         | Method | URL                                               |
| -------------------------------|:------:|:-------------------------------------------------:|
| List manufacturers             | GET    | ```http://localhost:8100/api/manufacturers/```    |
| Create a manufacturer          | POST   | ```http://localhost:8100/api/manufacturers/```    |
| Get a specific manufacturer    | GET    | ```http://localhost:8100/api/manufacturers/:id/```|
| Update a specific manufacturer | PUT    | ```http://localhost:8100/api/manufacturers/:id/```|
| Delete a specific manufacturer | DELETE | ```http://localhost:8100/api/manufacturers/:id/```|

example JSON input
```json
{
  "name": "Toyota"
}
```

example output
```json
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Toyota"
}
```

example list of manufacturers
```json
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Toyota"
    }
  ]
}
```

</br>
</br>

***Vehicle Model RESTful API endpoints***

| Action                  | Method | URL                                        |
| ------------------------|:------:|:------------------------------------------:|
| List models             | GET    | ```http://localhost:8100/api/models/```    |
| Create a model          | POST   | ```http://localhost:8100/api/models/```    |
| Get a specific model    | GET    | ```http://localhost:8100/api/models/:id/```|
| Update a specific model | PUT    | ```http://localhost:8100/api/models/:id/```|
| Delete a specific model | DELETE | ```http://localhost:8100/api/models/:id/```|


example JSON input
```json
{
  "name": "4runner",
  "picture_url": "https://images.pexels.com/photos/1683519/pexels-photo-1683519.jpeg?auto=compress&cs=tinysrgb&w=1600",
  "manufacturer_id": 1
}
```

example output
```json
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "4runner",
  "picture_url": "https://images.pexels.com/photos/1683519/pexels-photo-1683519.jpeg?auto=compress&cs=tinysrgb&w=1600",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Toyota"
  }
}
```

example list of models
```json
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "4runner",
      "picture_url": "https://images.pexels.com/photos/1683519/pexels-photo-1683519.jpeg?auto=compress&cs=tinysrgb&w=1600",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Toyota"
      }
    }
  ]
}
```

</br>
</br>

***Automobile RESTful API endpoints***

| Action                       | Method | URL                                              |
| -----------------------------|:------:|:------------------------------------------------:|
| List automobiles             | GET    | ```http://localhost:8100/api/automobiles/```     |
| Create a automobile          | POST   | ```http://localhost:8100/api/automobiles/```     |
| Get a specific automobile    | GET    | ```http://localhost:8100/api/automobiles/:vin/```|
| Update a specific automobile | PUT    | ```http://localhost:8100/api/automobiles/:vin/```|
| Delete a specific automobile | DELETE | ```http://localhost:8100/api/automobiles/:vin/```|

example JSON input
```json
{
  "color": "gold",
  "year": 1995,
  "vin": "AB1CD34GH45JKLMNO",
  "model_id": 1
}
```

example output
```json
{
  "href": "/api/automobiles/AB1CD34GH45JKLMNO/",
  "id": 1,
  "color": "gold",
  "year": 1995,
  "vin": "AB1CD34GH45JKLMNO",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "4runner",
    "picture_url": "https://images.pexels.com/photos/1683519/pexels-photo-1683519.jpeg?auto=compress&cs=tinysrgb&w=1600",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Toyota"
    }
  }
}
```

example list of automobiles
```json
{
  "autos": [
    {
      "href": "/api/automobiles/AB1CD34GH45JKLMNO/",
      "id": 1,
      "color": "gold",
      "year": 1995,
      "vin": "AB1CD34GH45JKLMNO",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "4runner",
        "picture_url": "https://images.pexels.com/photos/1683519/pexels-photo-1683519.jpeg?auto=compress&cs=tinysrgb&w=1600",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Toyota"
        }
      }
    }
  ]
}
```

</br>
</br>


## Service microservice

When looking at the Service microservice it has a total of 3 models. Where two are Entities and the last one is Value Object. In this service your able to create a Service appointment and create a technician. It also allow you to obtain a list of all Service appoinments and also allows you to filter by service id and also by automobile Vin number using correct API. The microservice is using a poller that is pulling from the inventory API in order to provide accurate date to the automobile value object service model. which provides the vin for the vip status.


Models for Service microservice:

1. AutoMoblieVO(Value Object)

  - Polling the inventory model information from there automoblie model
  - Properties = [ "vin", "color", "year", "model"]
  - only using vin at the moment but pulled all information so if we wanted to expand project.

2. Technician(Entitie)

  - Requires properties = ["name", "employee_number",]

3. ServiceAppointment(Entitie)

  - requires properties = ["customer_name", "date", "time", "reason", "completed", "vip", "canceled", "vin", "technician"(foreign key)]
  - pattern in insomnia for date would be: YYYY-DD-MM.
  - pattern in insomnia for time would be: HH:MM

__Service Port: 8080__

***Service RESTful API endpoints***

| Action                             | Method | URL                                    |
|------------------------------------|--------|----------------------------------------|
| List Service Appointments          | GET    | `http://localhost:8080/api/services/`    |
| Create Service Appointments        | POST   | `http://localhost:8080/api/services/` |
| Service Appointments Details       | GET    | `http://localhost:8080/api/services/<int:pk>/`|
| Edit Service Appointment           | PUT    | `http://localhost:8080/api/services/<int:pk>/`|
| Delete Service Appointment         | DELETE | `http://localhost:8080/api/services/<int:pk>/`|
| filter Service Appointments by vin | GET    | `http://localhost:8080/api/automobiles/<int:vin_vo>/services/`|


example JSON input
  - pattern in insomnia for date would be: YYYY-DD-MM.
  - pattern in insomnia for time would be: HH:MM
  - pattern for vin has to be 17 characters.


```json
  {
    "customer_name": "jeff",
    "date": "1998-07-08",
    "time": "12:30",
    "technician":  1,
    "reason": "oil",
    "vin": "12345678912345678"
  }

```

example output

```json
  {
    "customer_name": "jeff",
    "date": "1998-07-08T00:00:00",
    "time": "1900-01-01T12:00:30",
    "technician": {
      "name": "lets go",
      "employee_number": 54321,
      "id": 1
    },
    "reason": "oil",
    "vip": false,
    "vin": "12345678912345678",
    "completed": false,
    "id": 14
  }
```

example list of services and their details

```json

	"service_appointments": [
    {
      "customer_name": "Nick",
      "date": "2023-03-09T00:00:00+00:00",
      "time": "1900-01-01T10:00:09+00:00",
      "technician": {
        "name": "blue",
        "employee_number": 543212,
        "id": 5
      },
      "reason": "oil",
      "vip": false,
      "vin": "12345678912345678",
      "completed": false,
      "id": 10
    }
  ]

```

***Technicians RESTful API endpoints***

| Action                  | Method | URL                                    |
|-------------------------|--------|----------------------------------------|
| List  Technicians       | GET    | `http://localhost:8080/api/technician/`    |
| Create Technician       | POST   | `http://localhost:8080/api/technician/`    |
| List Technician details | GET    | `http://localhost:8080/api/technician/<int:pk>/`|
| Delete Technician       | DELETE | `http://localhost:8080/api/technician/<int:pk>/`|

example JSON input

```json
		{
			"name": "lob",
			"employee_number": 5859
		}
```

example output

```json
		{
			"name": "lets go",
			"employee_number": 54321,
			"id": 1
		},
```

example list of technicians and their details

```json
	"technicians": [
		{
			"name": "lets go",
			"employee_number": 54321,
			"id": 1
		},
  ]
```


## Sales microservice

The Sales microservice has four models, three Entities and one Value Object. It is capable of creating Customers, Sales Persons, and Sales Records. It is also able to list all sales and filter by sales person. This microservice connects to and polls the Inventory API in order to retrieve the automobile data which populates the automobile value object for this microservice which is then used for the Sales Record.

Within the Sales microservice there are four models:

1. AutomobileVO

    - Value Object populated by polling the Inventory microservice for the automobile model.
    - Pulling all info for further expansion, but only requires VIN currently.

2. Customer

    - Requires properties = [ "name", "address", "phone_number" ]

    - There is a pattern for the phone-number field you must follow: 'xxx-xxx-xxxx'

3. SalesPerson

    - Requires properties = [ "name", "employee_number" ]

4. SalesRecord

    - Requires properties = [ "automobileVO_ForeignKey", "sales_person_ForeignKey", "customer_ForeignKey", "price" ]

    - There is a pattern for the "price" field you must follow:
        - Requires a comma splitting every 3 digits and only allows up to two decimal places

__Sales Port: 8090__

***Available Automobiles For Sale RESTful API endpoints***

| Action                       | Method | URL                                                   |
| -----------------------------|:------:|:-----------------------------------------------------:|
| List automobiles for sale    | GET    | ```http://localhost:8090/api/available_automobiles/```|

example JSON output
```json
{
  "available_autos": [
    {
      "vin": "1C4GH5IB4ALK12198",
      "color": "grey",
      "year": 2001,
      "model": "Focus"
    },
  ]
}
```

</br>
</br>

***Customer RESTful API endpoints***

| Action                     | Method | URL                                           |
| ---------------------------|:------:|:---------------------------------------------:|
| List customers             | GET    | ```http://localhost:8090/api/customers/```    |
| Create a customer          | POST   | ```http://localhost:8090/api/customers/```    |
| Get a specific customer    | GET    | ```http://localhost:8090/api/customers/:id/```|
| Update a specific customer | PUT    | ```http://localhost:8090/api/customers/:id/```|
| Delete a specific customer | DELETE | ```http://localhost:8090/api/customers/:id/```|

example JSON input
```json
{
  "name": "John Baker",
  "address": "1234 Very Cool St.",
  "phone_number": "123-456-7899"
}
```

example output
```json
{
  "href": "/api/customers/1/",
  "id": 1,
  "name": "John Baker",
  "address": "1234 Very Cool St.",
  "phone_number": "123-456-7899"
}
```

example list of customers and their details
```json
{
  "customers": [
    {
      "href": "/api/customers/1/",
      "id": 1,
      "name": "John Baker",
      "address": "1234 Very Cool St.",
      "phone_number": "123-456-7899"
    },
  ]
}
```

</br>
</br>

***Sales Person RESTful API endpoints***

| Action                         | Method | URL                                               |
| -------------------------------|:------:|:-------------------------------------------------:|
| List sales persons             | GET    | ```http://localhost:8090/api/sales_persons/```    |
| Create a sales person          | POST   | ```http://localhost:8090/api/sales_persons/```    |
| Get a specific sales person    | GET    | ```http://localhost:8090/api/sales_persons/:id/```|
| Update a specific sales person | PUT    | ```http://localhost:8090/api/sales_persons/:id/```|
| Delete a specific sales person | DELETE | ```http://localhost:8090/api/sales_persons/:id/```|

example JSON input
```json
{
    "name": "Ricardo Montoya",
    "employee_number": 23
}
```

example output
```json
{
    "href": "/api/sales_persons/1/",
    "id": 1,
    "name": "Ricardo Montoya",
    "employee_number": 23
}
```

example list of sales persons
```json
{
  "sales_persons": [
    {
      "href": "/api/sales_persons/1/",
      "id": 1,
      "name": "Ricardo Montoya",
      "employee_number": 23
    },
  ]
}
```

</br>
</br>

***Sales Record RESTful API endpoints***

| Action                                      | Method | URL                                                                                   |
| --------------------------------------------|:------:|:-------------------------------------------------------------------------------------:|
| List sales records                          | GET    | ```http://localhost:8090/api/sales_records/```                                        |
| List sales person specific sales records    | GET    | ```http://localhost:8090/api/sales_persons/<int:employee_number>/sales_records/```    |
| Create a sales record                       | POST   | ```http://localhost:8090/api/sales_records/```                                        |
| Create a sales person specific sales record | POST   | ```http://localhost:8090/api/sales_persons/<int:employee_number>/sales_records/```    |
| Get a specific sales record                 | GET    | ```http://localhost:8090/api/sales_records/:id/```                                    |
| Update a specific sales record              | PUT    | ```http://localhost:8090/api/sales_records/:id/```                                    |
| Delete a specific sales record              | DELETE | ```http://localhost:8090/api/sales_records/:id/```                                    |

example JSON input
```json
{
  "automobile": "1C4GH5IB4ALK12198",
  "sales_person": 1,
  "customer": 1,
  "price": "1,000.00"
}
```

example output
```json
{
  "id": 1,
  "price": "1,000.00",
  "sales_person": {
    "id": 1,
    "name": "Ricardo Montoya",
    "employee_number": 23
  },
  "customer": {
    "id": 1,
    "name": "John Baker",
    "address": "1234 Very Cool St.",
    "phone_number": "123-456-7899"
  },
  "automobile": {
    "vin": "1C4GH5IB4ALK12198",
    "color": "grey",
    "year": 2001,
    "model": "Focus"
  }
}
```

example list of sales records
```json
{
  "sales_records": [
    {
      "id": 1,
      "price": "1,000.00",
      "sales_person": {
        "id": 1,
        "name": "Ricardo Montoya",
        "employee_number": 23
      },
      "customer": {
        "id": 1,
        "name": "John Baker",
        "address": "1234 Very Cool St",
        "phone_number": "123-456-7899"
      },
      "automobile": {
        "vin": "1C4GH5IB4ALK12198",
        "color": "grey",
        "year": 2001,
        "model": "Focus"
      }
    }
  ]
}
```
