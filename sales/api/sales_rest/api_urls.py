from django.urls import path

from .api_views import (
    api_available_automobiles,
    api_customers,
    api_customer,
    api_sales_persons,
    api_sales_person,
    api_sales_records,
    api_sales_record,
)


urlpatterns = [
    path("available_automobiles/", api_available_automobiles, name="api_available_automobiles"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:id>/", api_customer, name="api_customer"),
    path("sales_persons/", api_sales_persons, name="api_sales_persons"),
    path("sales_persons/<int:id>/", api_sales_person, name="api_sales_person"),
    path(
        "sales_persons/<int:sales_person_employee_number>/sales_records/",
        api_sales_records,
        name="api_employee_sales_records"
    ),
    path("sales_records/", api_sales_records, name="api_sales_records"),
    path("sales_records/<int:id>/", api_sales_record, name="api_sales_record"),
]
