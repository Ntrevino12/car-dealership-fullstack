from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
    SalesRecordEncoder,
)
from .models import (
    AutomobileVO,
    SalesPerson,
    Customer,
    SalesRecord,
)


# Create API for Available Automobiles for Sale
# Use Filter for lack of sales_record
@require_http_methods(["GET"])
def api_available_automobiles(request):
    if request.method == "GET":
        available_autos = AutomobileVO.objects.filter(sales_record__isnull=True)
        return JsonResponse(
            {"available_autos": available_autos},
            encoder=AutomobileVOEncoder,
        )


# Create SalesPerson List and Detail API
@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        try:
            sales_persons = SalesPerson.objects.all()
            return JsonResponse(
                {"sales_persons": sales_persons},
                encoder=SalesPersonEncoder,
            )
        except:
            return JsonResponse({"message": "There are no sales persons"})
    else: #POST
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create sales person"},
                status=400,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_person(request, id):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            count, _ = SalesPerson.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "No sales person to delete"})
    else: # PUT
        try:
            content = json.loads(request.body)
            SalesPerson.objects.filter(id=id).update(**content)
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )


# Create Customer List and Detail API
@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
            )
        except:
            return JsonResponse({"message": "There are no customers"})
    else: #POST
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create customer"},
                status=400,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "No customer to delete"},
                status=404,
            )
    else: #PUT
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=id).update(**content)
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )


# Create Sales Record List and Detail
@require_http_methods(["GET", "POST"])
def api_sales_records(request, sales_person_employee_number=None):
    if request.method == "GET":
        if sales_person_employee_number is not None:
            sales_records = SalesRecord.objects.filter(sales_person__employee_number=sales_person_employee_number)
        else:
            sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else: #POST
        content = json.loads(request.body)
        # try...except blocks for each Foreign Key
        try:
            # create through Sales Person ID is easiest
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404,
            )
        try:
            # create through Customer ID is easiest
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )
        try:
            # create through Automobile VIN is most proper
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=404
            )
        # Check if Sale Record already exists for Automobile VIN
        record_of_sale = SalesRecord.objects.filter(automobile__vin=content["automobile"])
        if record_of_sale:
            return JsonResponse(
                {"message": "This automobile has already been sold"},
                status=400,
            )
        else:
            try:
                sales_record = SalesRecord.objects.create(**content)
                return JsonResponse(
                    sales_record,
                    encoder=SalesRecordEncoder,
                    safe=False,
                )
            except:
                return JsonResponse(
                    {"message": "Could not create sales record"},
                    status=400,
                )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_record(request, id):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            count, _ = SalesRecord.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "No sales record to delete"},
                status=404,
            )
    else: #PUT
        content = json.loads(request.body)
        # try...except blocks for each Foreign Key
        try:
            if "sales_person" in content:
                sales_person = SalesPerson.objects.get(id=content["sales_person"])
                content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person ID"},
                status=400,
            )
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=400,
            )
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(vin=content["automobile"])
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile VIN"},
                status=400,
            )
        try:
            SalesRecord.objects.filter(id=id).update(**content)
            sales_record = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist"},
                status=404,
            )
