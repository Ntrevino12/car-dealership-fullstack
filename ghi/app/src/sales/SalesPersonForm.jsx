import React, {useEffect, useState} from 'react';


function SalesPersonForm () {
    // Create a bunch of useState hooks and Change functions for the different inputs we need
    const [name, setName] = useState('');
    const handleNameChange = (event) => setName(event.target.value);

    const [employeeNumber, setEmployeeNumber] = useState('');
    const handleEmployeeNumberChange = (event) => setEmployeeNumber(event.target.value);


    // onSubmit Handle event
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.employee_number = employeeNumber;

        const salespersonUrl = 'http://localhost:8090/api/sales_persons/';
        const fetchConfig ={
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();

            setName('');
            setEmployeeNumber('');
        }

    }


    // return Form JSX
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sales person</h1>
                    <form onSubmit={handleSubmit} id="create-sales-person-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                                type="text" name="name" id="name" value={name}
                                className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberChange} placeholder="Employee Number" required
                                type="number" name="employee_number" id="employee_number" value={employeeNumber}
                                className="form-control"/>
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm;
