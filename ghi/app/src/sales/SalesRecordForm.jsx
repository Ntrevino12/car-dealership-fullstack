import React, {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';


function SalesRecordForm () {
    const [load, setLoad] = useState(false);
    const navigate = useNavigate();

    // Create a bunch of useState hooks and Change functions for the different inputs we need
    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => setAutomobile(event.target.value);

    const [salesPerson, setSalesPerson] = useState('');
    const handleSalesPersonChange = (event) => setSalesPerson(event.target.value);

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => setCustomer(event.target.value);

    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => setPrice(event.target.value);

    const [automobiles, setAutomobiles] = useState([]);
    const [salesPersons, setSalesPersons] = useState([]);
    const [customers, setCustomers] = useState([]);


    // Fetch data for dropdown select fields
    const getAll = async () => {
        // Get Available Autos for Sale
        const automobilesUrl = 'http://localhost:8090/api/available_automobiles/';

        const automobilesResponse = await fetch(automobilesUrl);

        if (automobilesResponse.ok) {
            const autoData = await automobilesResponse.json();
            setAutomobiles(autoData.available_autos);
        }
        // Get Sales Persons
        const salesPersonsUrl = 'http://localhost:8090/api/sales_persons/';

        const salesPersonsResponse = await fetch(salesPersonsUrl);

        if (salesPersonsResponse.ok) {
            const salesPersonData = await salesPersonsResponse.json();
            setSalesPersons(salesPersonData.sales_persons);
        }
        // Get Customers
        const customersUrl = 'http://localhost:8090/api/customers/';

        const customersResponse = await fetch(customersUrl);

        if (customersResponse.ok) {
            const customerData = await customersResponse.json();
            setCustomers(customerData.customers);
        }
        // Load State Change
        if (true) {
            setLoad(!load);
        }
    };

    useEffect(() => {
        getAll();
    }, [load]);


    // onSubmit Handle event
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.automobile = automobile;
        data.sales_person = salesPerson;
        data.customer = customer;
        data.price = price;


        const salesRecordUrl = 'http://localhost:8090/api/sales_records/';
        const fetchConfig ={
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salesRecordUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();

            setAutomobile('');
            setSalesPerson('');
            setCustomer('');
            setPrice('');

            navigate("/sales_records/");
        }

    }


    // return Form JSX
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} required name="automobile" id="automobile" value={automobile} className="form-select">
                                <option value="">Choose an automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange} required name="sales_person" id="sales_person" value={salesPerson} className="form-select">
                                <option value="">Choose a sales person</option>
                                {salesPersons.map(salesPerson => {
                                    return (
                                        <option key={salesPerson.id} value={salesPerson.id}>
                                            {salesPerson.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" value={customer} className="form-select">
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Sales price" required
                                type="text" name="price" id="price" value={price}
                                className="form-control" pattern="^\d{1,3}(?:,\d{3})*\.\d{2}$"/>
                            <label htmlFor="price">Sales price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesRecordForm;
