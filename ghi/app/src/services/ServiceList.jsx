import React, { useEffect, useState} from 'react';


function ServiceList(){
    const [services, setServices] = useState([]);
    const [completed, setCompleted] = useState(false);



   const handleComplete = async (event) => {
        const serviceUrl = `http://localhost:8080/api/services/${event.id}/`
        const fetchOptions ={
            method:"put",
            body:JSON.stringify({completed:true}),
            headers:{
                'Content-type': 'application/json',
            }
        }
        const serviceResponse = await fetch(serviceUrl, fetchOptions);
        const data = await serviceResponse.json()
        if (serviceResponse.ok){
            setCompleted(true)
            setServices(service => {
                return service.filter(service => service.id !== event.id)
            })





        }
    }



    const fetchData  = async () => {
      const listUrl = `http://localhost:8080/api/services/`;
      const response = await fetch(listUrl);

      if (response.ok) {
        const data = await response.json();
        setServices(data.service_appointments);
      }
    }

    async function deleteService(services_appointments) {
        const deleteUrl = `http://localhost:8080/api/services/${services_appointments.id}/`
        const response = await fetch(deleteUrl, {method:"delete"})
        if (response.ok) {
            setServices(service => {
                return service.filter(service => service.id !== services_appointments.id)
            })

        }



    }



    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <h1 className=" text-center-mt-4">Service appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIP</th>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>time</th>
                        <th>Technician</th>
                        <th>reason</th>
                    </tr>
                </thead>
                <tbody>
                    {services.filter((service)=>{
                        return service.completed=== false
                    }).map((service) => {
                        if (service.vip === true){
                            return service.vip ="Yes"
                        }
                    return (
                        <tr key={service.id} value={service.id}>
                            <td>{service.vip}</td>
                            <td>{ service.vin }</td>
                            <td>{ service.customer_name }</td>
                            <td>{ new Date(service.date).toLocaleDateString() }</td>
                            <td>{ new Date (service.time).toLocaleTimeString([], { hour: "2-digit"}) }</td>
                            <td>{ service.technician.name}</td>
                            <td>{ service.reason }</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => deleteService(service)}>Cancel</button>
                                <button className="btn btn-success"onClick={() => handleComplete(service)} >Finished</button>
                            </td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
    }

export default ServiceList;
